/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>
#include <sys/stat.h>

#include "logging.h"
#include "port.h"
#include "ports.h"
#include "string.h"
#include "util.h"

struct nw_ports_entry {
	nw_port* port;

	// Link to the other entries
	STAILQ_ENTRY(nw_ports_entry) nodes;
};

struct nw_ports {
	nw_daemon* daemon;
	int nrefs;

	// Port Entries
	STAILQ_HEAD(entries, nw_ports_entry) entries;

	// A counter of the port entries
	unsigned int num;
};

int nw_ports_create(nw_ports** ports, nw_daemon* daemon) {
	nw_ports* p = calloc(1, sizeof(*p));
	if (!p)
		return 1;

	// Store a reference to the daemon
	p->daemon = nw_daemon_ref(daemon);

	// Initialize the reference counter
	p->nrefs = 1;

	// Initialize entries
	STAILQ_INIT(&p->entries);

	// Reference the pointer
	*ports = p;

	return 0;
}

static void nw_ports_free(nw_ports* ports) {
	struct nw_ports_entry* entry = NULL;

	while (!STAILQ_EMPTY(&ports->entries)) {
		entry = STAILQ_FIRST(&ports->entries);

		// Dereference the port
		nw_port_unref(entry->port);

		// Remove the entry from the list
		STAILQ_REMOVE_HEAD(&ports->entries, nodes);

		// Free the entry
		free(entry);
	}
}

nw_ports* nw_ports_ref(nw_ports* ports) {
	ports->nrefs++;

	return ports;
}

nw_ports* nw_ports_unref(nw_ports* ports) {
	if (--ports->nrefs > 0)
		return ports;

	nw_ports_free(ports);
	return NULL;
}

int nw_ports_save(nw_ports* ports) {
	struct nw_ports_entry* entry = NULL;
	int r;

	STAILQ_FOREACH(entry, &ports->entries, nodes) {
		r = nw_port_save(entry->port);
		if (r)
			return r;
	}

	return 0;
}

static int nw_ports_add_port(nw_ports* ports, nw_port* port) {
	// Allocate a new entry
	struct nw_ports_entry* entry = calloc(1, sizeof(*entry));
	if (!entry)
		return 1;

	// Reference the port
	entry->port = nw_port_ref(port);

	// Add it to the list
	STAILQ_INSERT_TAIL(&ports->entries, entry, nodes);

	// Increment the counter
	ports->num++;

	return 0;
}

static int __nw_ports_enumerate(struct dirent* entry, FILE* f, void* data) {
	nw_port* port = NULL;
	int r;

	nw_ports* ports = (nw_ports*)data;

	// Create a new port
	r = nw_port_open(&port, ports->daemon, entry->d_name, f);
	if (r < 0 || r == 1)
		goto ERROR;

	// Add the port to the list
	r = nw_ports_add_port(ports, port);
	if (r)
		goto ERROR;

ERROR:
	if (port)
		nw_port_unref(port);

	return r;
}

int nw_ports_enumerate(nw_ports* ports) {
	nw_configd* configd = NULL;
	int r;

	// Fetch ports configuration directory
	configd = nw_daemon_configd(ports->daemon, "ports");
	if (!configd)
		return 0;

	// Walk through all files
	r = nw_configd_walk(configd, __nw_ports_enumerate, ports);

	// Cleanup
	nw_configd_unref(configd);

	return r;
}

nw_port* nw_ports_get_by_name(nw_ports* ports, const char* name) {
	struct nw_ports_entry* entry = NULL;

	STAILQ_FOREACH(entry, &ports->entries, nodes) {
		const char* __name = nw_port_name(entry->port);

		// If the name matches, return a reference to the zone
		if (strcmp(name, __name) == 0)
			return nw_port_ref(entry->port);
	}

	// No match found
	return NULL;
}

int nw_ports_bus_paths(nw_ports* ports, char*** paths) {
	struct nw_ports_entry* entry = NULL;
	char* path = NULL;

	// Allocate an array for all paths
	char** p = calloc(ports->num + 1, sizeof(*p));
	if (!p)
		return 1;

	unsigned int i = 0;

	// Walk through all ports
	STAILQ_FOREACH(entry, &ports->entries, nodes) {
		// Generate the bus path
		path = nw_port_bus_path(entry->port);
		if (!path)
			goto ERROR;

		// Append the bus path to the array
		p[i++] = path;
	}

	// Return pointer
	*paths = p;

	return 0;

ERROR:
	if (p) {
		for (char** e = p; *e; e++)
			free(*e);
		free(p);
	}

	return 1;
}

int nw_ports_walk(nw_ports* ports, nw_ports_walk_callback callback, void* data) {
	struct nw_ports_entry* entry = NULL;
	int r;

	STAILQ_FOREACH(entry, &ports->entries, nodes) {
		r = callback(ports->daemon, entry->port, data);
		if (r)
			return r;
	}

	return 0;
}

static int __nw_ports_reconfigure(nw_daemon* daemon, nw_port* port, void* data) {
	return nw_port_reconfigure(port);
}

int nw_ports_reconfigure(nw_ports* ports) {
	return nw_ports_walk(ports, __nw_ports_reconfigure, NULL);
}
