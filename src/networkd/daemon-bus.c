/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdlib.h>

#include <systemd/sd-bus.h>

#include "bus.h"
#include "daemon.h"
#include "logging.h"
#include "port-bus.h"
#include "zone-bus.h"
#include "zones.h"

static int nw_daemon_bus_reload(sd_bus_message* m, void* data, sd_bus_error* error) {
	nw_daemon* daemon = (nw_daemon*)data;

	// Reload the daemon
	nw_daemon_reload(daemon);

	// Respond with an empty message
	return sd_bus_reply_method_return(m, NULL);
}

static int __nw_daemon_bus_list_ports(nw_daemon* daemon, nw_port* port, void* data) {
	sd_bus_message* reply = (sd_bus_message*)data;
	int r;

	// Fetch port name
	const char* name = nw_port_name(port);

	// Fetch bus path
	char* path = nw_port_bus_path(port);

	// Append the port to the message
	r = sd_bus_message_append(reply, "(so)", name, path);
	if (r < 0)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	if (path)
		free(path);

	return r;
}

static int nw_daemon_bus_list_ports(sd_bus_message* m, void* data, sd_bus_error* error) {
	nw_daemon* daemon = (nw_daemon*)data;
	sd_bus_message* reply = NULL;
	int r;

	// Form a reply message
	r = sd_bus_message_new_method_return(m, &reply);
	if (r < 0)
		goto ERROR;

	r = sd_bus_message_open_container(reply, 'a', "(so)");
	if (r < 0)
		goto ERROR;

	r = nw_daemon_ports_walk(daemon, __nw_daemon_bus_list_ports, reply);
	if (r < 0)
		goto ERROR;

	r = sd_bus_message_close_container(reply);
	if (r < 0)
		goto ERROR;

	// Send the reply
	r = sd_bus_send(NULL, reply, NULL);

ERROR:
	if (reply)
		sd_bus_message_unref(reply);

	return r;
}

static int __nw_daemon_bus_list_zones(nw_daemon* daemon, nw_zone* zone, void* data) {
	sd_bus_message* reply = (sd_bus_message*)data;
	int r;

	// Fetch zone name
	const char* name = nw_zone_name(zone);

	// Fetch bus path
	char* path = nw_zone_bus_path(zone);

	// Append the zone to the message
	r = sd_bus_message_append(reply, "(so)", name, path);
	if (r < 0)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	if (path)
		free(path);

	return r;
}

static int nw_daemon_bus_list_zones(sd_bus_message* m, void* data, sd_bus_error* error) {
	nw_daemon* daemon = (nw_daemon*)data;
	sd_bus_message* reply = NULL;
	int r;

	// Form a reply message
	r = sd_bus_message_new_method_return(m, &reply);
	if (r < 0)
		goto ERROR;

	r = sd_bus_message_open_container(reply, 'a', "(so)");
	if (r < 0)
		goto ERROR;

	r = nw_daemon_zones_walk(daemon, __nw_daemon_bus_list_zones, reply);
	if (r < 0)
		goto ERROR;

	r = sd_bus_message_close_container(reply);
	if (r < 0)
		goto ERROR;

	// Send the reply
	r = sd_bus_send(NULL, reply, NULL);

ERROR:
	if (reply)
		sd_bus_message_unref(reply);

	return r;
}

static const sd_bus_vtable daemon_vtable[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_METHOD_WITH_ARGS("ListPorts", SD_BUS_NO_ARGS, SD_BUS_RESULT("a(so)", links),
		nw_daemon_bus_list_ports, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD_WITH_ARGS("ListZones", SD_BUS_NO_ARGS, SD_BUS_RESULT("a(so)", links),
		nw_daemon_bus_list_zones, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD("Reload", SD_BUS_NO_ARGS, SD_BUS_NO_RESULT,
		nw_daemon_bus_reload, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_VTABLE_END,
};

const nw_bus_implementation daemon_bus_impl = {
	.path = "/org/ipfire/network1",
	.interface = "org.ipfire.network1",
	.vtables = BUS_VTABLES(daemon_vtable),
	.children = BUS_IMPLEMENTATIONS(&port_bus_impl, &zone_bus_impl),
};
