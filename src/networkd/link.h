/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_LINK_H
#define NETWORKD_LINK_H

#include <linux/if_link.h>

#include <systemd/sd-device.h>

typedef struct nw_link nw_link;

#include "daemon.h"
#include "json.h"

int nw_link_create(nw_link** link, nw_daemon* daemon, int ifindex);

nw_link* nw_link_ref(nw_link* link);
nw_link* nw_link_unref(nw_link* link);

int nw_link_ifindex(nw_link* link);
const char* nw_link_ifname(nw_link* link);

// Stats
const struct rtnl_link_stats64* nw_link_get_stats64(nw_link* link);
int nw_link_update_stats(nw_link* link);

int nw_link_has_carrier(nw_link* link);

int nw_link_process(sd_netlink* rtnl, sd_netlink_message* message, void* data);

int nw_link_destroy(nw_link* link);

// uevent
int nw_link_handle_uevent(nw_link* link, sd_device* device, sd_device_action_t action);

// JSON
int nw_link_to_json(nw_link* link, struct json_object* o);

#endif /* NETWORKD_LINK_H */
