/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_PORT_H
#define NETWORKD_PORT_H

#include <json.h>

#include <systemd/sd-netlink.h>

typedef struct nw_port nw_port;

typedef enum nw_port_type_id {
	NW_PORT_BONDING,
	NW_PORT_DUMMY,
	NW_PORT_ETHERNET,
	NW_PORT_VETH,
	NW_PORT_VLAN,
} nw_port_type_id_t;

typedef struct nw_port_type {
	// Type ID
	nw_port_type_id_t id;

	// IFLA_INFO_KIND/IFLA_INFO_DATA
	const char* kind;

	// Configuration
	int (*setup)(nw_port* port);
	int (*validate)(nw_port* port);

	// Get Parent Port
	nw_port* (*get_parent_port)(nw_port* port);

	// Link
	int (*create_link)(nw_port* port, sd_netlink_message* message);
	int (*destroy_link)(nw_port* port);

	// JSON
	int (*to_json)(nw_port* port, struct json_object* object);
} nw_port_type_t;

#include "address.h"
#include "config.h"
#include "daemon.h"
#include "json.h"
#include "port-bonding.h"
#include "port-ethernet.h"
#include "port-veth.h"
#include "port-vlan.h"

#define NW_PORT_TYPE(port) (port->type)

struct nw_port {
	nw_daemon* daemon;
	int nrefs;

	// Link
	nw_link* link;

	const nw_port_type_t* type;
	char name[IFNAMSIZ];

	// Configuration
	nw_config *config;

	// Common attributes
	nw_address_t address;

	// Bonding Settings
	struct nw_port_bonding bonding;

	// Ethernet Settings
	struct nw_port_ethernet ethernet;

	// VETH Settings
	struct nw_port_veth veth;

	// VLAN settings
	struct nw_port_vlan vlan;
};

int nw_port_create(nw_port** port, nw_daemon* daemon,
	const nw_port_type_id_t type, const char* name, nw_config* config);
int nw_port_open(nw_port** port, nw_daemon* daemon, const char* name, FILE* f);

nw_port* nw_port_ref(nw_port* port);
nw_port* nw_port_unref(nw_port* port);

int nw_port_destroy(nw_port* port);
int __nw_port_drop_port(nw_daemon* daemon, nw_port* port, void* data);

int nw_port_save(nw_port* port);

const char* nw_port_name(nw_port* port);

char* nw_port_bus_path(nw_port* port);

int __nw_port_set_link(nw_daemon* daemon, nw_port* port, void* data);
int __nw_port_drop_link(nw_daemon* daemon, nw_port* port, void* data);

const nw_address_t* nw_port_get_address(nw_port* port);

nw_port* nw_port_get_parent_port(nw_port* port);

int nw_port_reconfigure(nw_port* port);

int nw_port_has_carrier(nw_port* port);

int nw_port_check_type(nw_port* port, const nw_port_type_id_t type);

// Stats
const struct rtnl_link_stats64* nw_port_get_stats64(nw_port* port);
int __nw_port_update_stats(nw_daemon* daemon, nw_port* port, void* data);
int nw_port_update_stats(nw_port* port);

// JSON
int nw_port_to_json(nw_port* port, struct json_object** object);

#endif /* NETWORKD_PORT_H */
