/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <linux/if_link.h>

#include <systemd/sd-netlink.h>

#include "config.h"
#include "daemon.h"
#include "json.h"
#include "logging.h"
#include "port.h"
#include "port-vlan.h"
#include "string.h"

const nw_string_table_t nw_port_vlan_proto[] = {
	{ NW_VLAN_PROTO_8021Q,  "802.1Q" },
	{ NW_VLAN_PROTO_8021AD, "802.1ad" },
	{ -1, NULL },
};

NW_STRING_TABLE_LOOKUP(nw_port_vlan_proto_t, nw_port_vlan_proto)

static int nw_port_vlan_setup(nw_port* port) {
	int r;

	// VLAN ID
	r = NW_CONFIG_OPTION_INT(port->config, "VLAN_ID", &port->vlan.id);
	if (r < 0)
		return r;

	// VLAN Protocol
	r = NW_CONFIG_OPTION_STRING_TABLE(port->config,
			"VLAN_PROTO", &port->vlan.proto, nw_port_vlan_proto);
	if (r < 0)
		return r;

	// Parent Port
	r = NW_CONFIG_OPTION_STRING_BUFFER(port->config,
		"VLAN_PARENT", port->vlan.__parent_name);
	if (r < 0)
		return r;

	return 0;
}

static int nw_port_vlan_validate(nw_port* port) {
	// Check if the VLAN ID is within range
	if (port->vlan.id < NW_VLAN_ID_MIN || port->vlan.id > NW_VLAN_ID_MAX) {
		ERROR("%s: Invalid VLAN ID %d\n", port->name, port->vlan.id);
		return 1;
	}

	// Validate protocol
	switch (port->vlan.proto) {
		case NW_VLAN_PROTO_8021Q:
		case NW_VLAN_PROTO_8021AD:
			break;

		default:
			ERROR("%p: Invalid VLAN protocol\n", port->name);
			return 1;
	}

	return 0;
}

static int nw_port_vlan_create_link(nw_port* port, sd_netlink_message* m) {
	int r;

	// Set VLAN ID
	r = sd_netlink_message_append_u16(m, IFLA_VLAN_ID, port->vlan.id);
	if (r < 0)
		return r;

	// Set VLAN protocol
	r = sd_netlink_message_append_u16(m, IFLA_VLAN_PROTOCOL, htobe16(port->vlan.proto));
	if (r < 0)
		return r;

	return 0;
}

static int nw_port_vlan_to_json(nw_port* port, struct json_object* o) {
	nw_port* parent = NULL;
	int r;

	// Add the VLAN ID
	r = json_object_add_int64(o, "VLANId", port->vlan.id);
	if (r < 0)
		goto ERROR;

	// Add the VLAN Protocol
	r = json_object_add_string(o, "VLANProtocol",
			nw_port_vlan_proto_to_string(port->vlan.proto));
	if (r < 0)
		goto ERROR;

	// Fetch the parent
	parent = nw_port_get_parent_port(port);
	if (parent) {
		r = json_object_add_string(o, "VLANParentPort", nw_port_name(parent));
		if (r < 0)
			goto ERROR;
	}

ERROR:
	if (parent)
		nw_port_unref(parent);

	return r;
}

const nw_port_type_t nw_port_type_vlan = {
	.kind = "vlan",

	// Configuration
	.setup = nw_port_vlan_setup,
	.validate = nw_port_vlan_validate,

	.get_parent_port = nw_port_get_vlan_parent,

	// Link
	.create_link = nw_port_vlan_create_link,

	// JSON
	.to_json = nw_port_vlan_to_json,
};

/*
	VLAN
*/
int nw_port_get_vlan_id(nw_port* port) {
	int r;

	// Check type
	r = nw_port_check_type(port, NW_PORT_VLAN);
	if (r < 0)
		return r;

	return port->vlan.id;
}

int nw_port_set_vlan_id(nw_port* port, int id) {
	int r;

	// Check type
	r = nw_port_check_type(port, NW_PORT_VLAN);
	if (r < 0)
		return r;

	// Check if the VLAN ID is within range
	if (id < NW_VLAN_ID_MIN || id > NW_VLAN_ID_MAX)
		return -EINVAL;

	// Store the ID
	port->vlan.id = id;

	DEBUG("Port %s: Set VLAN ID to %d\n", port->name, port->vlan.id);

	return 0;
}

int nw_port_set_vlan_proto(nw_port* port, const nw_port_vlan_proto_t proto) {
	switch (proto) {
		case NW_VLAN_PROTO_8021Q:
		case NW_VLAN_PROTO_8021AD:
			port->vlan.proto = proto;
			break;

		default:
			return -EINVAL;
	}

	return 0;
}

nw_port* nw_port_get_vlan_parent(nw_port* port) {
	int r;

	// Check type
	r = nw_port_check_type(port, NW_PORT_VLAN);
	if (r < 0)
		return NULL;

	// Try to find a reference to the parent if none exists
	if (!port->vlan.parent && *port->vlan.__parent_name)
		port->vlan.parent = nw_daemon_get_port_by_name(port->daemon, port->vlan.__parent_name);

	if (port->vlan.parent)
		return nw_port_ref(port->vlan.parent);

	// No port found
	return NULL;
}

int nw_port_set_vlan_parent(nw_port* port, nw_port* parent) {
	int r;

	// Check type
	r = nw_port_check_type(port, NW_PORT_VLAN);
	if (r < 0)
		return r;

	// Reset the former parent name
	nw_string_empty(port->vlan.__parent_name);

	// Dereference the former parent
	if (port->vlan.parent) {
		nw_port_unref(port->vlan.parent);
		port->vlan.parent = NULL;
	}

	// Store the new parent
	if (parent) {
		port->vlan.parent = nw_port_ref(parent);

		// Store the name
		nw_string_set(port->vlan.__parent_name, nw_port_name(parent));
	}

	DEBUG("Port %s: Set VLAN parent to %s\n", port->name, nw_port_name(port->vlan.parent));

	return 0;
}
