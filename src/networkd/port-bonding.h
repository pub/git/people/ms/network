/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_PORT_BONDING_H
#define NETWORKD_PORT_BONDING_H

#include <linux/if_bonding.h>

#include "port.h"

typedef enum nw_port_bonding_mode {
	NW_BONDING_MODE_ROUNDROBIN   = BOND_MODE_ROUNDROBIN,
	NW_BONDING_MODE_ACTIVEBACKUP = BOND_MODE_ACTIVEBACKUP,
	NW_BONDING_MODE_XOR          = BOND_MODE_XOR,
	NW_BONDING_MODE_BROADCAST    = BOND_MODE_BROADCAST,
	NW_BONDING_MODE_8023AD       = BOND_MODE_8023AD,
	NW_BONDING_MODE_TLB          = BOND_MODE_TLB,
	NW_BONDING_MODE_ALB          = BOND_MODE_ALB,
} nw_port_bonding_mode_t;

struct nw_port_bonding {
	nw_port_bonding_mode_t mode;
};

extern const nw_port_type_t nw_port_type_bonding;

const char* nw_port_bonding_get_mode(nw_port* port);
int nw_port_bonding_set_mode(nw_port* port, const char* mode);

#endif /* NETWORKD_PORT_BONDING_H */
