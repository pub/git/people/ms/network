/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include <systemd/sd-journal.h>

#include "logging.h"

void nw_log(int priority, const char* file,
		int line, const char* fn, const char* format, ...) {
	char* buffer = NULL;
	va_list args;
	int r;

	// Format log message
	va_start(args, format);
	r = vasprintf(&buffer, format, args);
	va_end(args);
	if (r < 0)
		return;

	// Send message to journald
	r = sd_journal_send(
		"MESSAGE=%s", buffer,
		"PRIORITY=%d", priority,

		// Syslog compat
		"SYSLOG_IDENTIFIER=networkd",

		// Debugging stuff
		"ERRNO=%d", errno,
		"CODE_FILE=%s", file,
		"CODE_LINE=%d", line,
		"CODE_FUNC=%s", fn,

		NULL
	);

	// Fall back to standard output
	if (r)
		sd_journal_perror(buffer);

	// Cleanup
	free(buffer);
}
