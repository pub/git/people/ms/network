/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_ADDRESS_H
#define NETWORKD_ADDRESS_H

#include <errno.h>
#include <netinet/ether.h>
#include <string.h>
#include <sys/random.h>

#include "logging.h"

typedef struct ether_addr nw_address_t;

enum {
	NW_ADDRESS_MULTICAST        = (1 << 0),
	NW_ADDRESS_SOFTWAREASSIGNED = (1 << 1),
};

static inline int nw_address_from_string(nw_address_t* addr, const char* s) {
	if (!s)
		return -EINVAL;

	struct ether_addr* p = ether_aton_r(s, addr);
	if (!p)
		return -errno;

	return 0;
}

static inline char* nw_address_to_string(const nw_address_t* addr) {
	char buffer[20];

	char* p = ether_ntoa_r(addr, buffer);
	if (!p)
		return NULL;

	return strdup(buffer);
}

static inline int nw_address_generate(nw_address_t* addr) {
	ssize_t bytes = getrandom(addr, sizeof(*addr), 0);
	if (bytes < 0) {
		ERROR("getrandom() failed: %m\n");
		return 1;
	}

	// Check if we filled the entire buffer
	if (bytes < (ssize_t)sizeof(*addr)) {
		ERROR("Could not gather enough randomness\n");
		return 1;
	}

	// Clear the multicast bit
	addr->ether_addr_octet[0] &= ~NW_ADDRESS_MULTICAST;

	// Set the software-generated bit
	addr->ether_addr_octet[0] |= NW_ADDRESS_SOFTWAREASSIGNED;

	return 0;
}

static inline int nw_address_is_multicast(const nw_address_t* addr) {
	return (addr->ether_addr_octet[0] & NW_ADDRESS_MULTICAST);
}

#endif /* NETWORKD_ADDRESS_H */
