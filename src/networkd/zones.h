/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_ZONES_H
#define NETWORKD_ZONES_H

typedef struct nw_zones nw_zones;

typedef int (*nw_zones_walk_callback)(nw_daemon* daemon, nw_zone* zone, void* data);

#include "daemon.h"

int nw_zones_create(nw_zones** zones, nw_daemon* daemon);

nw_zones* nw_zones_ref(nw_zones* zones);
nw_zones* nw_zones_unref(nw_zones* zones);

int nw_zones_save(nw_zones* zones);

int nw_zones_enumerate(nw_zones* zones);

size_t nw_zones_num(nw_zones* zones);

nw_zone* nw_zones_get_by_name(nw_zones* zones, const char* name);

int nw_zones_bus_paths(nw_zones* zones, char*** paths);

int nw_zones_walk(nw_zones* zones, nw_zones_walk_callback callback, void* data);

int nw_zones_reconfigure(nw_zones* zones);

#endif /* NETWORKD_ZONES_H */
