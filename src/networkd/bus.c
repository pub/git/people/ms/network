/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdlib.h>

#include <systemd/sd-bus.h>
#include <systemd/sd-event.h>

#include "bus.h"
#include "daemon.h"
#include "daemon-bus.h"
#include "logging.h"

static int nw_bus_on_connect(sd_bus_message* m, void* data, sd_bus_error* error) {
	nw_daemon* daemon = (nw_daemon*)data;

	DEBUG("Connected to D-Bus\n");

	return 0;
}

int nw_bus_connect(sd_bus** bus, sd_event* loop, nw_daemon* daemon) {
	sd_bus* b = NULL;
	int r;

	// Create a bus object
	r = sd_bus_new(&b);
	if (r < 0) {
		ERROR("Could not allocate a bus object: %s\n", strerror(-r));
		return 1;
	}

	// Set description
	r = sd_bus_set_description(b, NETWORKD_BUS_DESCRIPTION);
	if (r < 0) {
		ERROR("Could not set bus description: %s\n", strerror(-r));
		return 1;
	}

	const char* address = secure_getenv("DBUS_SYSTEM_BUS_ADDRESS");
	if (!address)
		address = DEFAULT_SYSTEM_BUS_ADDRESS;

	// Set bus address
	r = sd_bus_set_address(b, address);
	if (r < 0) {
		ERROR("Could not set bus address: %s\n", strerror(-r));
		return 1;
	}

	// Set bus client
	r = sd_bus_set_bus_client(b, 1);
	if (r < 0) {
		ERROR("Could not set bus client: %s\n", strerror(-r));
		return 1;
	}

	// Request some credentials for all messages
	r = sd_bus_negotiate_creds(b, 1,
			SD_BUS_CREDS_UID|SD_BUS_CREDS_EUID|SD_BUS_CREDS_EFFECTIVE_CAPS);
	if (r < 0) {
		ERROR("Could not negotiate creds: %s\n", strerror(-r));
		return 1;
	}

	// Automatically bind when the socket is available
	r = sd_bus_set_watch_bind(b, 1);
	if (r < 0) {
		ERROR("Could not watch socket: %s\n", strerror(-r));
		return 1;
	}

	// Emit a connected signal when we are connected
	r = sd_bus_set_connected_signal(b, 1);
	if (r < 0) {
		ERROR("Could not enable sending a connect signal: %s\n", strerror(-r));
		return 1;
	}

	// Connect to the bus
	r = sd_bus_start(b);
	if (r < 0) {
		ERROR("Could not connect to bus: %s\n", strerror(-r));
		return 1;
	}

	// Register the implementation
	r = nw_bus_register_implementation(b, &daemon_bus_impl, daemon);
	if (r)
		return r;

	// Request interface name
	r = sd_bus_request_name_async(b, NULL, "org.ipfire.network1", 0, NULL, NULL);
	if (r < 0) {
		ERROR("Could not request bus name: %s\n", strerror(-r));
		return 1;
	}

	// Attach the event loop
	r = sd_bus_attach_event(b, loop, 0);
	if (r < 0) {
		ERROR("Could not attach bus to event loop: %s\n", strerror(-r));
		return 1;
	}

	// Request receiving a connect signal
	r = sd_bus_match_signal_async(b, NULL, "org.freedesktop.DBus.Local",
		NULL, "org.freedesktop.DBus.Local", "Connected", nw_bus_on_connect, NULL, NULL);
	if (r < 0) {
		ERROR("Could not request match on Connected signal: %s\n", strerror(-r));
		return 1;
	}

	// Return reference
	*bus = b;

	return 0;
}

int nw_bus_register_implementation(sd_bus* bus,
		const struct nw_bus_implementation* impl, void* data) {
	int r;

	DEBUG("Registering bus object implementation for path=%s iface=%s\n",
		impl->path, impl->interface);

	// Register vtables
	for (const sd_bus_vtable** vtable = impl->vtables; vtable && *vtable; vtable++) {
		r = sd_bus_add_object_vtable(bus, NULL, impl->path, impl->interface, *vtable, data);
		if (r < 0) {
			ERROR("Could not register bus path %s with interface %s: %m\n",
				impl->path, impl->interface);
			return 1;
		}
	}

	// Register fallback vtables
	for (const struct nw_bus_vtable_pair* p = impl->fallback_vtables; p && p->vtable; p++) {
		r = sd_bus_add_fallback_vtable(bus, NULL, impl->path, impl->interface,
				p->vtable, p->object_find, data);
		if (r < 0) {
			ERROR("Could not register bus path %s with interface %s: %m\n",
				impl->path, impl->interface);
			return 1;
		}
	}

	// Register the node enumerator
	if (impl->node_enumerator) {
		r = sd_bus_add_node_enumerator(bus, NULL, impl->path, impl->node_enumerator, data);
		if (r < 0) {
			ERROR("Could not add the node enumerator for %s: %m\n", impl->path);
			return 1;
		}
	}

	// Register any child implementations
	for (int i = 0; impl->children && impl->children[i]; i++) {
		r = nw_bus_register_implementation(bus, impl->children[i], data);
		if (r)
			return r;
	}

	return 0;
}
