/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_JSON_H
#define NETWORKD_JSON_H

#include <errno.h>
#include <string.h>

#include <json.h>

// Give some sane names to the reference count functions
#define json_object_ref json_object_get
#define json_object_unref json_object_put

static inline int __json_object_add(struct json_object* o,
		const char* key, struct json_object* value) {
	int r;

	// Add the object
	r = json_object_object_add(o, key, value);
	if (r < 0) {
		if (value)
			json_object_unref(value);
	}

	return r;
}

static inline int json_object_add_string(struct json_object* o,
		const char* key, const char* value) {
	struct json_object* element = NULL;

	// Create a JSON object from the string
	element = json_object_new_string(value);
	if (!element)
		return -errno;

	return __json_object_add(o, key, element);
}

static inline int json_object_add_int64(struct json_object* o,
		const char* key, const int64_t value) {
	struct json_object* element = NULL;

	// Create a JSON object
	element = json_object_new_int64(value);
	if (!element)
		return -errno;

	return __json_object_add(o, key, element);
}

static inline int json_to_string(struct json_object* o, char** s, size_t* l) {
	// Format JSON to string
	const char* buffer = json_object_to_json_string_ext(o,
		JSON_C_TO_STRING_PRETTY|JSON_C_TO_STRING_PRETTY_TAB);
	if (!buffer)
		return -errno;

	// Copy the string to the heap
	*s = strdup(buffer);
	if (!*s)
		return -errno;

	// If requested, store the length of the string
	if (l)
		*l = strlen(*s);

	return 0;
}

static inline const char* json_object_fetch_string(
		struct json_object* o, const char* key) {
	struct json_object* e = json_object_object_get(o, key);
	if (!e)
		return NULL;

	return json_object_get_string(e);
}

#endif /* NETWORKD_JSON_H */
