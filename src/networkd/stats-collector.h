/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_STATS_COLLECTOR_H
#define NETWORKD_STATS_COLLECTOR_H

#include <systemd/sd-event.h>

#include "daemon.h"
#include "port.h"
#include "zone.h"

#define NW_STATS_COLLECTOR_INTERVAL			15 * 1000000ULL // 15 sec in µsec

int nw_stats_collector(sd_event_source* s, long unsigned int usec, void* data);

int nw_stats_collector_emit_port_stats(nw_daemon* daemon, nw_port* port);
int nw_stats_collector_emit_zone_stats(nw_daemon* daemon, nw_zone* zone);

#endif /* NETWORKD_STATS_COLLECTOR_H */
