/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdlib.h>

#include <systemd/sd-bus.h>
#include <systemd/sd-event.h>

#include "logging.h"
#include "port.h"
#include "stats-collector.h"
#include "zone.h"

static int __nw_stats_collector_port(nw_daemon* daemon, nw_port* port, void* data) {
	return nw_port_update_stats(port);
}

static int __nw_stats_collector_zone(nw_daemon* daemon, nw_zone* zone, void* data) {
	return nw_zone_update_stats(zone);
}

int nw_stats_collector(sd_event_source* s, long unsigned int usec, void* data) {
	nw_daemon* daemon = (nw_daemon*)data;
	int r;

	DEBUG("Stats collector has been called\n");

	// Schedule the next call
	r = sd_event_source_set_time(s, usec + NW_STATS_COLLECTOR_INTERVAL);
	if (r < 0)
		return r;

	// Ports
	r = nw_daemon_ports_walk(daemon, __nw_stats_collector_port, NULL);
	if (r)
		return r;

	// Zones
	r = nw_daemon_zones_walk(daemon, __nw_stats_collector_zone, NULL);
	if (r)
		return r;

	return 0;
}

static int nw_stats_collector_emit_stats(nw_daemon* daemon, const char* path,
		const char* interface, const char* member, const struct rtnl_link_stats64* stats64) {
	sd_bus_message* m = NULL;
	int r;

	sd_bus* bus = nw_daemon_get_bus(daemon);

	// Allocate a new message
	r = sd_bus_message_new_signal(bus, &m, path, interface, member);
	if (r < 0) {
		errno = -r;
		ERROR("Could not allocate bus message: %m\n");
		goto ERROR;
	}

	// Open the container
	r = sd_bus_message_open_container(m, 'a', "{st}");
	if (r < 0) {
		ERROR("Could not open container: %m\n");
		goto ERROR;
	}

	const struct stats64_entry {
		const char* key;
		uint64_t value;
	} entries[] = {
		{ "rx-packets",          stats64->rx_packets },
		{ "tx-packets",          stats64->tx_packets },
		{ "rx-bytes",            stats64->rx_bytes },
		{ "tx-bytes",            stats64->tx_bytes },
		{ "rx-errors",           stats64->rx_errors },
		{ "tx-errors",           stats64->tx_errors },
		{ "rx-dropped",          stats64->rx_dropped },
		{ "tx-dropped",          stats64->tx_dropped },
		{ "multicast",           stats64->multicast },
		{ "collisions",          stats64->collisions },

		// Detailed RX errors
		{ "rx-length-errors",    stats64->rx_length_errors },
		{ "rx-over-errors",      stats64->rx_over_errors },
		{ "rx-crc-errors",       stats64->rx_crc_errors },
		{ "rx-frame-errors",     stats64->rx_frame_errors },
		{ "rx-fifo-errors",      stats64->rx_fifo_errors },
		{ "rx-missed-errors",    stats64->rx_missed_errors },

		// Detailed TX errors
		{ "tx-aborted-errors",   stats64->tx_aborted_errors },
		{ "tx-carrier-errors",   stats64->tx_carrier_errors },
		{ "tx-fifo-errors",      stats64->tx_fifo_errors },
		{ "tx-heartbeat-errors", stats64->tx_heartbeat_errors },
		{ "tx-window-errors",    stats64->tx_window_errors },

		{ NULL },
	};

	for (const struct stats64_entry* e = entries; e->key; e++) {
		r = sd_bus_message_append(m, "{st}", e->key, e->value);
		if (r < 0) {
			ERROR("Could not set stat value: %m\n");
			goto ERROR;
		}
	}

	// Close the container
	r = sd_bus_message_close_container(m);
	if (r < 0) {
		ERROR("Could not close container: %m\n");
		goto ERROR;
	}

	// Emit the signal
	r = sd_bus_send(bus, m, NULL);
	if (r < 0) {
		ERROR("Could not emit the stats signal for %s: %m\n", path);
		goto ERROR;
	}

ERROR:
	if (m)
		sd_bus_message_unref(m);

	return r;
}

int nw_stats_collector_emit_port_stats(nw_daemon* daemon, nw_port* port) {
	const struct rtnl_link_stats64* stats64 = NULL;
	char* path = NULL;
	int r;

	// Fetch the bus path
	path = nw_port_bus_path(port);

	// Fetch the stats
	stats64 = nw_port_get_stats64(port);

	// Emit the stats
	r = nw_stats_collector_emit_stats(daemon, path,
		"org.ipfire.network1.Port", "Stats", stats64);
	if (r < 0) {
		ERROR("Could not emit stats for port %s: %m\n", nw_port_name(port));
		goto ERROR;
	}

ERROR:
	if (path)
		free(path);

	return r;
}

int nw_stats_collector_emit_zone_stats(nw_daemon* daemon, nw_zone* zone) {
	const struct rtnl_link_stats64* stats64 = NULL;
	char* path = NULL;
	int r;

	// Fetch the bus path
	path = nw_zone_bus_path(zone);

	// Fetch the stats
	stats64 = nw_zone_get_stats64(zone);

	// Emit the stats
	r = nw_stats_collector_emit_stats(daemon, path,
		"org.ipfire.network1.Zone", "Stats", stats64);
	if (r < 0) {
		ERROR("Could not emit stats for zone %s: %m\n", nw_zone_name(zone));
		goto ERROR;
	}

ERROR:
	if (path)
		free(path);

	return r;
}
