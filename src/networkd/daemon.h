/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_DAEMON_H
#define NETWORKD_DAEMON_H

#include <dirent.h>
#include <stdio.h>

#include <systemd/sd-bus.h>
#include <systemd/sd-netlink.h>

typedef struct nw_daemon nw_daemon;

#include "config.h"
#include "link.h"
#include "links.h"
#include "port.h"
#include "ports.h"
#include "zone.h"
#include "zones.h"

int nw_daemon_create(nw_daemon** daemon, int argc, char* argv[]);

nw_daemon* nw_daemon_ref(nw_daemon* daemon);
nw_daemon* nw_daemon_unref(nw_daemon* daemon);

int nw_daemon_run(nw_daemon* daemon);

int nw_daemon_reload(nw_daemon* daemon);

int nw_daemon_save(nw_daemon* daemon);

nw_configd* nw_daemon_configd(nw_daemon* daemon, const char* path);

/*
	Bus
*/
sd_bus* nw_daemon_get_bus(nw_daemon* daemon);

/*
	Netlink
*/
sd_netlink* nw_daemon_get_rtnl(nw_daemon* daemon);

/*
	Links
*/
nw_links* nw_daemon_links(nw_daemon* daemon);
void nw_daemon_drop_link(nw_daemon* daemon, nw_link* link);
nw_link* nw_daemon_get_link_by_ifindex(nw_daemon* daemon, int ifindex);
nw_link* nw_daemon_get_link_by_name(nw_daemon* daemon, const char* name);

/*
	Ports
*/
nw_ports* nw_daemon_ports(nw_daemon* daemon);
int nw_daemon_ports_walk(nw_daemon* daemon, nw_ports_walk_callback callback, void* data);
nw_port* nw_daemon_get_port_by_name(nw_daemon* daemon, const char* name);

/*
	Zones
*/
nw_zones* nw_daemon_zones(nw_daemon* daemon);
int nw_daemon_zones_walk(nw_daemon* daemon, nw_zones_walk_callback callback, void* data);
nw_zone* nw_daemon_get_zone_by_name(nw_daemon* daemon, const char* name);

#endif /* NETWORKD_DAEMON_H */
