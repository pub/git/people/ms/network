/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_PORTS_H
#define NETWORKD_PORTS_H

typedef struct nw_ports nw_ports;

typedef int (*nw_ports_walk_callback)(nw_daemon* daemon, nw_port* port, void* data);

#include "daemon.h"
#include "port.h"

int nw_ports_create(nw_ports** ports, nw_daemon* daemon);

nw_ports* nw_ports_ref(nw_ports* ports);
nw_ports* nw_ports_unref(nw_ports* ports);

int nw_ports_save(nw_ports* ports);

int nw_ports_enumerate(nw_ports* ports);

struct nw_port* nw_ports_get_by_name(nw_ports* ports, const char* name);

int nw_ports_bus_paths(nw_ports* ports, char*** paths);

int nw_ports_walk(nw_ports* ports, nw_ports_walk_callback callback, void* data);

int nw_ports_reconfigure(nw_ports* ports);

#endif /* NETWORKD_PORTS_H */
