/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_PORT_VLAN_H
#define NETWORKD_PORT_VLAN_H

#include <arpa/inet.h>
#include <linux/if_ether.h>

#include "port.h"

typedef enum nw_port_vlan_proto {
	NW_VLAN_PROTO_8021Q  = ETH_P_8021Q,
	NW_VLAN_PROTO_8021AD = ETH_P_8021AD,
} nw_port_vlan_proto_t;

// VLAN ID
#define NW_VLAN_ID_INVALID		0
#define NW_VLAN_ID_MIN			1
#define NW_VLAN_ID_MAX			4096

struct nw_port_vlan {
	nw_port* parent;

	// The VLAN ID
	int id;

	// Protocol
	nw_port_vlan_proto_t proto;

	// If the parent has not been read from the configuration we will
	// save the name and try to find it later.
	char __parent_name[IFNAMSIZ];
};

extern const nw_port_type_t nw_port_type_vlan;

// ID
int nw_port_get_vlan_id(nw_port* port);
int nw_port_set_vlan_id(nw_port* port, int id);

// Protocol
int nw_port_set_vlan_proto(nw_port* port, const nw_port_vlan_proto_t proto);

// Parent Port
nw_port* nw_port_get_vlan_parent(nw_port* port);
int nw_port_set_vlan_parent(nw_port* port, nw_port* parent);

#endif /* NETWORKD_PORT_VLAN_H */
