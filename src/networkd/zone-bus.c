/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>

#include "bus.h"
#include "daemon.h"
#include "logging.h"
#include "zone.h"
#include "zone-bus.h"
#include "zones.h"

static int nw_zone_node_enumerator(sd_bus* bus, const char* path, void* data,
		char*** nodes, sd_bus_error* error) {
	int r;

	DEBUG("Enumerating zones...\n");

	// Fetch a reference to the daemon
	nw_daemon* daemon = (nw_daemon*)data;

	// Fetch zones
	nw_zones* zones = nw_daemon_zones(daemon);

	// Make bus paths for all zones
	r = nw_zones_bus_paths(zones, nodes);
	if (r)
		goto ERROR;

ERROR:
	nw_zones_unref(zones);

	return r;
}

static int nw_zone_object_find(sd_bus* bus, const char* path, const char* interface,
		void* data, void** found, sd_bus_error* error) {
	char* name = NULL;
	int r;

	// Fetch a reference to the daemon
	nw_daemon* daemon = (nw_daemon*)data;

	// Decode the path of the requested object
	r = sd_bus_path_decode(path, "/org/ipfire/network1/zone", &name);
	if (r <= 0)
		return 0;

	// Find the zone
	nw_zone* zone = nw_daemon_get_zone_by_name(daemon, name);
	if (!zone)
		return 0;

	// Match!
	*found = zone;

	nw_zone_unref(zone);

	return 1;
}

/*
	MTU
*/
static int nw_zone_bus_get_mtu(sd_bus* bus, const char *path, const char *interface,
		const char* property, sd_bus_message* reply, void* data, sd_bus_error *error) {
	nw_zone* zone = (nw_zone*)data;

	return sd_bus_message_append(reply, "u", nw_zone_mtu(zone));
}

static int nw_zone_bus_set_mtu(sd_bus* bus, const char* path, const char* interface,
		const char* property, sd_bus_message* value, void* data, sd_bus_error* error) {
	unsigned int mtu = 0;
	int r;

	nw_zone* zone = (nw_zone*)data;

	// Parse the value
	r = sd_bus_message_read(value, "u", &mtu);
	if (r < 0)
		return r;

	if (!nw_zone_set_mtu(zone, mtu))
		return -errno;

	return 0;
}

static const sd_bus_vtable zone_vtable[] = {
	SD_BUS_VTABLE_START(0),

	// MTU
	SD_BUS_WRITABLE_PROPERTY("MTU", "u", nw_zone_bus_get_mtu, nw_zone_bus_set_mtu,
		0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),

	SD_BUS_VTABLE_END
};

const nw_bus_implementation zone_bus_impl = {
	"/org/ipfire/network1/zone",
	"org.ipfire.network1.Zone",
	.fallback_vtables = BUS_FALLBACK_VTABLES({zone_vtable, nw_zone_object_find}),
	.node_enumerator = nw_zone_node_enumerator,
};
