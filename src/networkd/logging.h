/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_LOGGING_H
#define NETWORKD_LOGGING_H

#include <syslog.h>

void nw_log(int priority, const char *file, int line, const char* fn,
	const char *format, ...) __attribute__((format(printf, 5, 6)));

/*
	This is just something simple which will work for now...
*/
#define INFO(args...)  nw_log(LOG_INFO, __FILE__, __LINE__, __FUNCTION__, ## args)
#define WARNING(args...)  nw_log(LOG_WARNING, __FILE__, __LINE__, __FUNCTION__, ## args)
#define ERROR(args...) nw_log(LOG_ERR, __FILE__, __LINE__, __FUNCTION__, ## args)
#define DEBUG(args...) nw_log(LOG_DEBUG, __FILE__, __LINE__, __FUNCTION__, ## args)

#endif /* NETWORKD_LOGGING_H */
