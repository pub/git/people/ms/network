/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>

#include "daemon.h"
#include "logging.h"
#include "string.h"
#include "util.h"
#include "zone.h"
#include "zones.h"

struct nw_zones_entry {
	nw_zone* zone;

	// Link to the other entries
	STAILQ_ENTRY(nw_zones_entry) nodes;
};

struct nw_zones {
	nw_daemon* daemon;
	int nrefs;

	// Zone Entries
	STAILQ_HEAD(entries, nw_zones_entry) entries;

	// A counter of the zone entries
	unsigned int num;
};

int nw_zones_create(nw_zones** zones, nw_daemon* daemon) {
	nw_zones* z = calloc(1, sizeof(*z));
	if (!z)
		return 1;

	// Store a reference to the daemon
	z->daemon = nw_daemon_ref(daemon);

	// Initialize the reference counter
	z->nrefs = 1;

	// Initialize entries
	STAILQ_INIT(&z->entries);

	// Reference the pointer
	*zones = z;

	return 0;
}

static void nw_zones_free(nw_zones* zones) {
	struct nw_zones_entry* entry = NULL;

	while (!STAILQ_EMPTY(&zones->entries)) {
		entry = STAILQ_FIRST(&zones->entries);

		// Dereference the zone
		nw_zone_unref(entry->zone);

		// Remove the entry from the list
		STAILQ_REMOVE_HEAD(&zones->entries, nodes);

		// Free the entry
		free(entry);
	}

	if (zones->daemon)
		nw_daemon_unref(zones->daemon);

	free(zones);
}

nw_zones* nw_zones_ref(nw_zones* zones) {
	zones->nrefs++;

	return zones;
}

nw_zones* nw_zones_unref(nw_zones* zones) {
	if (--zones->nrefs > 0)
		return zones;

	nw_zones_free(zones);
	return NULL;
}

int nw_zones_save(nw_zones* zones) {
	struct nw_zones_entry* entry = NULL;
	int r;

	STAILQ_FOREACH(entry, &zones->entries, nodes) {
		r = nw_zone_save(entry->zone);
		if (r)
			return r;
	}

	return 0;
}

static int nw_zones_add_zone(nw_zones* zones, nw_zone* zone) {
	// Allocate a new entry
	struct nw_zones_entry* entry = calloc(1, sizeof(*entry));
	if (!entry)
		return 1;

	// Reference the zone
	entry->zone = nw_zone_ref(zone);

	// Add it to the list
	STAILQ_INSERT_TAIL(&zones->entries, entry, nodes);

	// Increment the counter
	zones->num++;

	return 0;
}

static int __nw_zones_enumerate(struct dirent* entry, FILE* f, void* data) {
	nw_zone* zone = NULL;
	int r;

	nw_zones* zones = (nw_zones*)data;

	// Create a new zone
	r = nw_zone_open(&zone, zones->daemon, entry->d_name, f);
	if (r < 0 || r == 1)
		goto ERROR;

	// Add the zone to the list
	r = nw_zones_add_zone(zones, zone);
	if (r)
		goto ERROR;

ERROR:
	if (zone)
		nw_zone_unref(zone);

	return r;
}

int nw_zones_enumerate(nw_zones* zones) {
	nw_configd* configd = NULL;
	int r;

	// Fetch zones configuration directory
	configd = nw_daemon_configd(zones->daemon, "zones");
	if (!configd)
		return 0;

	// Walk through all files
	r = nw_configd_walk(configd, __nw_zones_enumerate, zones);

	// Cleanup
	nw_configd_unref(configd);

	return r;
}

size_t nw_zones_num(nw_zones* zones) {
	struct nw_zones_entry* entry = NULL;
	size_t length = 0;

	// Count all zones
	STAILQ_FOREACH(entry, &zones->entries, nodes)
		length++;

	return length;
}

nw_zone* nw_zones_get_by_name(nw_zones* zones, const char* name) {
	struct nw_zones_entry* entry = NULL;

	STAILQ_FOREACH(entry, &zones->entries, nodes) {
		const char* __name = nw_zone_name(entry->zone);

		// If the name matches, return a reference to the zone
		if (strcmp(name, __name) == 0)
			return nw_zone_ref(entry->zone);
	}

	// No match found
	return NULL;
}

int nw_zones_bus_paths(nw_zones* zones, char*** paths) {
	struct nw_zones_entry* entry = NULL;
	char* path = NULL;

	// Allocate an array for all paths
	char** p = calloc(zones->num + 1, sizeof(*p));
	if (!p)
		return 1;

	unsigned int i = 0;

	// Walk through all zones
	STAILQ_FOREACH(entry, &zones->entries, nodes) {
		// Generate the bus path
		path = nw_zone_bus_path(entry->zone);
		if (!path)
			goto ERROR;

		// Append the bus path to the array
		p[i++] = path;
	}

	// Return pointer
	*paths = p;

	return 0;

ERROR:
	if (p) {
		for (char** e = p; *e; e++)
			free(*e);
		free(p);
	}

	return 1;
}

int nw_zones_walk(nw_zones* zones, nw_zones_walk_callback callback, void* data) {
	struct nw_zones_entry* entry = NULL;
	int r;

	STAILQ_FOREACH(entry, &zones->entries, nodes) {
		r = callback(zones->daemon, entry->zone, data);
		if (r)
			return r;
	}

	return 0;
}

static int __nw_zones_reconfigure(nw_daemon* daemon, nw_zone* zone, void* data) {
	return nw_zone_reconfigure(zone);
}

int nw_zones_reconfigure(nw_zones* zones) {
	return nw_zones_walk(zones, __nw_zones_reconfigure, NULL);
}
