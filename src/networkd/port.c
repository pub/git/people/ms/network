/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <limits.h>
#include <net/if.h>
#include <stdint.h>
#include <stdlib.h>

#include <systemd/sd-bus.h>

#include "address.h"
#include "config.h"
#include "json.h"
#include "link.h"
#include "logging.h"
#include "port.h"
#include "port-bonding.h"
#include "port-dummy.h"
#include "port-ethernet.h"
#include "port-veth.h"
#include "port-vlan.h"
#include "stats-collector.h"
#include "string.h"

static const nw_string_table_t nw_port_type_id[] = {
	{ NW_PORT_BONDING,  "bonding" },
	{ NW_PORT_DUMMY,    "dummy" },
	{ NW_PORT_ETHERNET, "ethernet" },
	{ NW_PORT_VETH,     "veth", },
	{ NW_PORT_VLAN,     "vlan" },
	{ -1, NULL },
};

NW_STRING_TABLE_LOOKUP(nw_port_type_id_t, nw_port_type_id)

static void nw_port_free(nw_port* port) {
	if (port->link)
		nw_link_unref(port->link);
	if (port->config)
		nw_config_unref(port->config);
	if (port->daemon)
		nw_daemon_unref(port->daemon);

	free(port);
}

static int nw_port_set_link(nw_port* port, nw_link* link) {
	// Do nothing if the same link is being re-assigned
	if (port->link == link)
		return 0;

	// Dereference the former link if set
	if (port->link)
		nw_link_unref(port->link);

	// Store the new link
	if (link) {
		port->link = nw_link_ref(link);

		DEBUG("Port %s: Assigned link %d\n", port->name, nw_link_ifindex(port->link));

	// Or clear the pointer if no link has been provided
	} else {
		port->link = NULL;

		DEBUG("Port %s: Removed link\n", port->name);
	}

	return 0;
}

static int nw_port_setup(nw_port* port) {
	nw_link* link = NULL;
	int r;

	// Find the link
	link = nw_daemon_get_link_by_name(port->daemon, port->name);
	if (link) {
		r = nw_port_set_link(port, link);
		if (r)
			goto ERROR;
	}

	// Generate a random Ethernet address
	r = nw_address_generate(&port->address);
	if (r < 0) {
		ERROR("Could not generate an Ethernet address: %s\n", strerror(-r));
		goto ERROR;
	}

	// Setup options
	r = NW_CONFIG_OPTION_ADDRESS(port->config, "ADDRESS", &port->address);
	if (r < 0)
		goto ERROR;

	// Call any custom initialization
	if (NW_PORT_TYPE(port)->setup) {
		r = NW_PORT_TYPE(port)->setup(port);
		if (r < 0)
			goto ERROR;
	}

	// Parse the configuration
	r = nw_config_options_read(port->config);
	if (r < 0) {
		ERROR("Could not read configuration for port %s: %s\n", port->name, strerror(-r));
		goto ERROR;
	}

ERROR:
	if (link)
		nw_link_unref(link);

	return r;
}

static int nw_port_validate(nw_port* port) {
	int r = 0;

	// Validate the port configuration
	if (NW_PORT_TYPE(port)->validate) {
		r = NW_PORT_TYPE(port)->validate(port);
		if (r < 0)
			ERROR("Could not check configuration for %s: %s\n", port->name, strerror(-r));
	}

	return r;
}

int nw_port_create(nw_port** port, nw_daemon* daemon,
		const nw_port_type_id_t type, const char* name, nw_config* config) {
	int r;

	// Allocate a new object
	nw_port* p = calloc(1, sizeof(*p));
	if (!p)
		return -errno;

	// Store a reference to the daemon
	p->daemon = nw_daemon_ref(daemon);

	// Initialize reference counter
	p->nrefs = 1;

	// Set operations
	switch (type) {
		case NW_PORT_BONDING:
			p->type = &nw_port_type_bonding;
			break;

		case NW_PORT_DUMMY:
			p->type = &nw_port_type_dummy;
			break;

		case NW_PORT_ETHERNET:
			p->type = &nw_port_type_ethernet;
			break;

		case NW_PORT_VETH:
			p->type = &nw_port_type_veth;
			break;

		case NW_PORT_VLAN:
			p->type = &nw_port_type_vlan;
			break;
	}

	// Store the name
	r = nw_string_set(p->name, name);
	if (r < 0)
		goto ERROR;

	// Copy the configuration
	r = nw_config_copy(config, &p->config);
	if (r < 0)
		goto ERROR;

	// Setup the port
	r = nw_port_setup(p);
	if (r < 0)
		goto ERROR;

	// Validate the configuration
	r = nw_port_validate(p);
	switch (r) {
		// Configuration is valid
		case 0:
			break;

		// Configuration is invalid
		case 1:
			ERROR("%s: Invalid configuration\n", p->name);
			goto ERROR;

		// Error
		default:
			goto ERROR;
	}

	*port = p;
	return 0;

ERROR:
	nw_port_free(p);
	return r;
}

int nw_port_open(nw_port** port, nw_daemon* daemon, const char* name, FILE* f) {
	nw_config* config = NULL;
	int r;

	// Initialize the configuration
	r = nw_config_create(&config, f);
	if (r < 0)
		goto ERROR;

	// Fetch the type
	const char* type = nw_config_get(config, "TYPE");
	if (!type) {
		ERROR("Port %s has no TYPE\n", name);
		r = -ENOTSUP;
		goto ERROR;
	}

	// Create a new port
	r = nw_port_create(port, daemon, nw_port_type_id_from_string(type), name, config);
	if (r < 0)
		goto ERROR;

ERROR:
	if (config)
		nw_config_unref(config);

	return r;
}

nw_port* nw_port_ref(nw_port* port) {
	port->nrefs++;

	return port;
}

nw_port* nw_port_unref(nw_port* port) {
	if (--port->nrefs > 0)
		return port;

	nw_port_free(port);
	return NULL;
}

/*
	This is a helper function for when we pass a reference to the event loop
	it will have to dereference the port instance later.
*/
static void __nw_port_unref(void* data) {
	nw_port* port = (nw_port*)data;

	nw_port_unref(port);
}

int nw_port_destroy(nw_port* port) {
	nw_configd* configd = NULL;
	int r;

	DEBUG("Destroying port %s\n", port->name);

	// Destroy the physical link (if exists)
	if (port->link) {
		r = nw_link_destroy(port->link);
		if (r < 0)
			goto ERROR;
	}

	// Dereference the port from other ports
	r = nw_daemon_ports_walk(port->daemon, __nw_port_drop_port, port);
	if (r < 0)
		goto ERROR;

	// Dereference the port from other zones
	r = nw_daemon_zones_walk(port->daemon, __nw_zone_drop_port, port);
	if (r < 0)
		goto ERROR;

	// Fetch the configuration directory
	configd = nw_daemon_configd(port->daemon, "ports");
	if (configd) {
		r = nw_configd_unlink(configd, port->name, 0);
		if (r < 0)
			goto ERROR;
	}

ERROR:
	if (configd)
		nw_configd_unref(configd);

	return r;
}

int __nw_port_drop_port(nw_daemon* daemon, nw_port* port, void* data) {
	nw_port* dropped_port = (nw_port*)data;
	int r;

	switch (port->type->id) {
		case NW_PORT_VLAN:
			if (port->vlan.parent == dropped_port) {
				r = nw_port_set_vlan_parent(port, NULL);
				if (r)
					return r;
			}
			break;

		case NW_PORT_BONDING:
		case NW_PORT_DUMMY:
		case NW_PORT_ETHERNET:
		case NW_PORT_VETH:
			break;
	}

	return 0;
}

int nw_port_save(nw_port* port) {
	nw_configd* configd = NULL;
	FILE* f = NULL;
	int r;

	// Fetch configuration directory
	configd = nw_daemon_configd(port->daemon, "ports");
	if (!configd) {
		r = -errno;
		goto ERROR;
	}

	// Open file
	f = nw_configd_fopen(configd, port->name, "w");
	if (!f) {
		r = -errno;
		goto ERROR;
	}

	// Write out the configuration
	r = nw_config_options_write(port->config);
	if (r < 0)
		goto ERROR;

	// Write the configuration
	r = nw_config_write(port->config, f);
	if (r < 0)
		goto ERROR;

ERROR:
	if (configd)
		nw_configd_unref(configd);
	if (f)
		fclose(f);
	if (r)
		ERROR("Could not save configuration for port %s: %s\n", port->name, strerror(-r));

	return r;
}

const char* nw_port_name(nw_port* port) {
	return port->name;
}

char* nw_port_bus_path(nw_port* port) {
	char* p = NULL;
	int r;

	// Encode the bus path
	r = sd_bus_path_encode("/org/ipfire/network1/port", port->name, &p);
	if (r < 0)
		return NULL;

	return p;
}

int __nw_port_set_link(nw_daemon* daemon, nw_port* port, void* data) {
	nw_link* link = (nw_link*)data;

	// Fetch the link name
	const char* ifname = nw_link_ifname(link);
	if (!ifname) {
		ERROR("Link does not have a name set\n");
		return 1;
	}

	// Set link if the name matches
	if (strcmp(port->name, ifname) == 0)
		return nw_port_set_link(port, link);

	// If we have the link set but the name did not match, we will remove it
	else if (port->link == link)
		return nw_port_set_link(port, NULL);

	return 0;
}

int __nw_port_drop_link(nw_daemon* daemon, nw_port* port, void* data) {
	nw_link* link = (nw_link*)data;

	// Drop the link if it matches
	if (port->link == link)
		return nw_port_set_link(port, NULL);

	return 0;
}

static nw_link* nw_port_get_link(nw_port* port) {
	// Fetch the link if not set
	if (!port->link)
		port->link = nw_daemon_get_link_by_name(port->daemon, port->name);

	if (!port->link)
		return NULL;

	return nw_link_ref(port->link);
}

const nw_address_t* nw_port_get_address(nw_port* port) {
	return &port->address;
}

static int nw_port_is_disabled(nw_port* port) {
	return nw_config_get_bool(port->config, "DISABLED");
}

nw_port* nw_port_get_parent_port(nw_port* port) {
	if (!NW_PORT_TYPE(port)->get_parent_port)
		return NULL;

	return NW_PORT_TYPE(port)->get_parent_port(port);
}

static nw_link* nw_port_get_parent_link(nw_port* port) {
	nw_port* parent = NULL;
	nw_link* link = NULL;

	// Fetch the parent
	parent = nw_port_get_parent_port(port);
	if (!parent)
		return NULL;

	// Fetch the link
	link = nw_port_get_link(parent);

	// Cleanup
	if (parent)
		nw_port_unref(parent);

	return link;
}

static int __nw_port_create_link(sd_netlink* rtnl, sd_netlink_message* m, void* data) {
	nw_port* port = (nw_port*)data;
	int r;

	// Check if the operation was successful
	r = sd_netlink_message_get_errno(m);
	if (r < 0) {
		ERROR("Could not create port %s: %s\n", port->name, strerror(-r));
		// XXX We should extract the error message

		return 0;
	}

	DEBUG("Successfully created %s\n", port->name);

	return 0;
}

static int nw_port_create_link(nw_port* port) {
	sd_netlink_message* m = NULL;
	nw_link* link = NULL;
	int r;

	sd_netlink* rtnl = nw_daemon_get_rtnl(port->daemon);

	DEBUG("Creating port %s...\n", port->name);

	// Check the kind
	if (!NW_PORT_TYPE(port)->kind) {
		ERROR("Port type has no kind\n");
		r = -ENOTSUP;
		goto ERROR;
	}

	// Create a new link
	r = sd_rtnl_message_new_link(rtnl, &m, RTM_NEWLINK, 0);
	if (r < 0) {
		ERROR("Could not create netlink message: %m\n");
		goto ERROR;
	}

	// Set the name
	r = sd_netlink_message_append_string(m, IFLA_IFNAME, port->name);
	if (r < 0) {
		ERROR("Could not set port name: %s\n", strerror(-r));
		goto ERROR;
	}

	// XXX Set common things like MTU, etc.

	// Set Ethernet address
	r = sd_netlink_message_append_ether_addr(m, IFLA_ADDRESS, &port->address);
	if (r < 0) {
		ERROR("Could not set MAC address: %s\n", strerror(-r));
		goto ERROR;
	}

	// Fetch the parent link
	link = nw_port_get_parent_link(port);
	if (link) {
		r = sd_netlink_message_append_u32(m, IFLA_LINK, nw_link_ifindex(link));
		if (r < 0)
			goto ERROR;
	}

	// Open an IFLA_LINKINFO container
	r = sd_netlink_message_open_container(m, IFLA_LINKINFO);
	if (r < 0)
		goto ERROR;

	// Run the custom setup
	if (NW_PORT_TYPE(port)->create_link) {
		r = sd_netlink_message_open_container_union(m, IFLA_INFO_DATA, NW_PORT_TYPE(port)->kind);
		if (r < 0) {
			ERROR("Could not open IFLA_INFO_DATA container: %s\n", strerror(-r));
			goto ERROR;
		}

		r = NW_PORT_TYPE(port)->create_link(port, m);
		if (r) {
			ERROR("Could not create port %s: %m\n", port->name);
			goto ERROR;
		}

		// Close the container
		r = sd_netlink_message_close_container(m);
		if (r < 0)
			goto ERROR;

	// Just set IFLA_INFO_KIND if there is no custom function
	} else {
		r = sd_netlink_message_append_string(m, IFLA_INFO_KIND, NW_PORT_TYPE(port)->kind);
		if (r < 0)
			goto ERROR;
	}

	// Close the container
	r = sd_netlink_message_close_container(m);
	if (r < 0)
		goto ERROR;

	// Send the message
	r = sd_netlink_call_async(rtnl, NULL, m, __nw_port_create_link,
		__nw_port_unref, nw_port_ref(port), -1, NULL);
	if (r < 0) {
		ERROR("Could not send netlink message: %s\n", strerror(-r));
		goto ERROR;
	}

	r = 0;

ERROR:
	if (m)
		sd_netlink_message_unref(m);
	if (link)
		nw_link_unref(link);

	return r;
}

int nw_port_reconfigure(nw_port* port) {
	int r;

	// If the port is disabled, we will try to destroy it
	if (nw_port_is_disabled(port)) {
		if (port->link) {
			r = nw_link_destroy(port->link);
			if (r)
				return r;
		}

		return 0;
	}

	// If there is no link, we will try to create it
	if (!port->link)
		return nw_port_create_link(port);

	// XXX TODO

	return 0;
}

int nw_port_has_carrier(nw_port* port) {
	if (!port->link)
		return 0;

	return nw_link_has_carrier(port->link);
}

/*
	Stats
*/

const struct rtnl_link_stats64* nw_port_get_stats64(nw_port* port) {
	if (!port->link)
		return NULL;

	return nw_link_get_stats64(port->link);
}

int __nw_port_update_stats(nw_daemon* daemon, nw_port* port, void* data) {
	nw_link* link = (nw_link*)data;

	// Emit stats if link matches
	if (port->link == link)
		return nw_stats_collector_emit_port_stats(daemon, port);

	return 0;
}

int nw_port_update_stats(nw_port* port) {
	if (port->link)
		return nw_link_update_stats(port->link);

	return 0;
}

int nw_port_check_type(nw_port* port, const nw_port_type_id_t type) {
	if (port->type->id == type)
		return 0;

	errno = ENOTSUP;
	return -errno;
}

/*
	JSON
*/
int nw_port_to_json(nw_port* port, struct json_object** object) {
	char* address = NULL;
	int r;

	// Create a new JSON object
	struct json_object* o = json_object_new_object();
	if (!o) {
		r = -errno;
		goto ERROR;
	}

	// Add name
	r = json_object_add_string(o, "Name", port->name);
	if (r < 0)
		goto ERROR;

	// Add Type
	r = json_object_add_string(o, "Type", nw_port_type_id_to_string(port->type->id));
	if (r < 0)
		goto ERROR;

	// Add address
	address = nw_address_to_string(&port->address);
	if (address) {
		r = json_object_add_string(o, "Address", address);
		if (r < 0)
			goto ERROR;
	}

	// Add link stuff
	if (port->link) {
		r = nw_link_to_json(port->link, o);
		if (r < 0)
			goto ERROR;
	}

	// Call custom stuff
	if (NW_PORT_TYPE(port)->to_json) {
		r = NW_PORT_TYPE(port)->to_json(port, o);
		if (r < 0)
			goto ERROR;
	}

	// Success
	r = 0;

	// Return a reference to the created object
	*object = json_object_ref(o);

ERROR:
	if (address)
		free(address);
	if (o)
		json_object_unref(o);

	return r;
}
