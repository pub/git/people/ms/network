/*#############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2023 IPFire Network Development Team                          #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef NETWORKD_ZONE_H
#define NETWORKD_ZONE_H

#define NETWORK_ZONE_DEFAULT_MTU			1500

typedef struct nw_zone nw_zone;

typedef enum nw_zone_type_id {
	__EMPTY
} nw_zone_type_id_t;

#include <linux/if.h>
#include <linux/if_link.h>

#include "config.h"
#include "daemon.h"
#include "link.h"

struct nw_zone {
	nw_daemon* daemon;
	int nrefs;

	// Link
	nw_link* link;

	char name[IFNAMSIZ];

	// Configuration
	nw_config *config;
};

int nw_zone_create(nw_zone** zone, nw_daemon* daemon, const nw_zone_type_id_t type,
	const char* name, nw_config* config);
int nw_zone_open(nw_zone** zone, nw_daemon* daemon, const char* name, FILE* f);

nw_zone* nw_zone_ref(nw_zone* zone);
nw_zone* nw_zone_unref(nw_zone* zone);

int __nw_zone_drop_port(nw_daemon* daemon, nw_zone* zone, void* data);

int nw_zone_save(nw_zone* zone);

const char* nw_zone_name(nw_zone* zone);

char* nw_zone_bus_path(nw_zone* zone);

int __nw_zone_set_link(nw_daemon* daemon, nw_zone* zone, void* data);
int __nw_zone_drop_link(nw_daemon* daemon, nw_zone* zone, void* data);

int nw_zone_reconfigure(nw_zone* zone);

int nw_zone_has_carrier(nw_zone* zone);

/*
	MTU
*/
unsigned int nw_zone_mtu(nw_zone* zone);
int nw_zone_set_mtu(nw_zone* zone, unsigned int mtu);

const struct rtnl_link_stats64* nw_zone_get_stats64(nw_zone* zone);
int __nw_zone_update_stats(nw_daemon* daemon, nw_zone* zone, void* data);
int nw_zone_update_stats(nw_zone* zone);

#endif /* NETWORKD_ZONE_H */
